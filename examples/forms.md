
```html
<FormGroup :initialValue="data" @change="handleChange" @submit="handleSubmit">

  <div class="form-group">
    <FormField type="text" name="name" placeholder="Name"></FormField>
  </div>
  
  <div class="form-group">
    <FormField type="checkbox" name="public" placeholder="Public"></FormField>
  </div>

  <button type="submit">Save</button>
</FormGroup>
```
