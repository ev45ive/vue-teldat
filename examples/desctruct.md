```ts
var person = {
    name:'Alice',
    address:{
        city:'Racoon',
        street:'Sezamkowa'
    },
    company:{
        name:'ACME'
    },
}

getInfo = person => {
    var {
        name, 
        address:{
            city, 
            postcode = ''
        }, 
        skills: [skill] = [],
        company:{
            name:companyName
        }
    } = person;
    
    return `Welcome ${name}, ${companyName} company in ${city}`
}

getInfo2 = ({
    name, 
    address:{
        city, 
        postcode = ''
    }, 
    skills: [skill] = [],
    company:{
        name:companyName
    }
} ) =>  `Welcome ${name}, ${companyName} company in ${city}`;

```