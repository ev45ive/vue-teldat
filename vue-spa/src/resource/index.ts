
import Vue from "vue";
import VueResource from 'vue-resource'
import { Http, HttpOptions, HttpResponse } from 'vue-resource/types/vue_resource';
import { auth } from '@/services/index';

Vue.use(VueResource, {});

declare module "vue/types/vue" {
  interface VueConstructor {
    http: any;
  }
}

Vue.http.options = {
  root: 'https://api.spotify.com/v1/',
};


Vue.http.interceptors.push(function (request:HttpOptions) {
  request.headers.set('Authorization', 'Bearer '+auth.getToken());

  return (response:HttpResponse) => {
    if(!response.ok){
      if(response.status == 401){
        auth.authorize()
      }
    }
    return response
  }
});


const http = {}
export default http;