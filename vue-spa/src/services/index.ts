import { Auth } from "./Auth.service";
import { PlaylistsService } from './PlaylistsService';

export const auth = new Auth({
  auth_url: 'https://accounts.spotify.com/authorize',
  client_id: 'a6e66100bf6247faa430f9e9256c47c9',
  redirect_uri: 'http://localhost:8080/oauth.html',
  response_type: 'token',
  scopes: [
    'playlist-read-collaborative',
    'playlist-modify-public',
    'playlist-read-private',
    'playlist-modify-private',
  ],
  show_dialog: false,
  state: ''
})

// export const playlistsService = new PlaylistsService()