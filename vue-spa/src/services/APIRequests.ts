import Vue from 'vue';
import { HttpOptions, HttpResponse } from 'vue-resource/types/vue_resource';
import { Observable } from "rxjs";


export class APIRequests {

  get(url: string, options?: HttpOptions): Observable<HttpResponse> {

    return new Observable<HttpResponse>(subscriber => {
      let request: { abort: () => void; };

      Promise.resolve(
        Vue.http.get(url, {
          ...options,
          before: (req: any) => (request = req)
        })
      )
        .then(resp => subscriber.next(resp))
        .catch(error => subscriber.error(error))
        .finally(() => subscriber.complete());

      return () => request.abort();
    });
  }

  post(url: string, data: any, options?: HttpOptions): Observable<HttpResponse> {

    return new Observable<HttpResponse>(subscriber => {
      let request: { abort: () => void; };

      Promise.resolve(
        Vue.http.post(url, data, {
          ...options,
          before: (req: any) => (request = req)
        })
      )
        .then(resp => subscriber.next(resp))
        .catch(error => subscriber.error(error))
        .finally(() => subscriber.complete());

      return () => request.abort();
    });
  }

  put(url: string, data: any, options?: HttpOptions): Observable<HttpResponse> {

    return new Observable<HttpResponse>(subscriber => {
      let request: { abort: () => void; };

      Promise.resolve(
        Vue.http.put(url, data, {
          ...options,
          before: (req: any) => (request = req)
        })
      )
        .then(resp => subscriber.next(resp))
        .catch(error => subscriber.error(error))
        .finally(() => subscriber.complete());

      return () => request.abort();
    });
  }
}
