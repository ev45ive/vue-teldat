
export interface AuthConfig {
  auth_url: string
  /**
   * The client ID provided to you by Spotify when you register your application.
   */
  client_id: string,
  /**
   * Set it to “token”.
   */
  response_type: 'token' | 'code',
  /**
   * The URI to redirect to after the user grants/denies permission. This URI needs to be entered in the URI whitelist that you specify when you register your application.
   */
  redirect_uri: string,
  /**
   but strongly recommended. The state can be useful for correlating requests and responses. Because your redirect_uri can be guessed, using a state value can increase your assurance that an incoming connection is the result of an authentication request. If you generate a random string or encode the hash of some client state (e.g., a cookie) in this state variable, you can validate the response to additionally ensure that the request and response originated in the same browser. This provides protection against attacks such as cross-site request forgery. See RFC-6749.
   */
  state: string,
  /**
   * A space - separated list of scopes: see Using Scopes.
   */
  scopes: string[]
  /**
  Whether or not to force the user to approve the app again if they’ve already done so.If false(default ), a user who has already approved the application may be automatically redirected to the URI specified by redirect_uri.If true, the user will not be automatically redirected and will have to approve the app again.
     */
  show_dialog: boolean
}

export class Auth {
  token: null | string = null;

  constructor(private config: AuthConfig) {
    this.extractToken()
  }

  init() { }

  authorize() {
    sessionStorage.removeItem('token')

    const { client_id, redirect_uri, response_type, scopes, show_dialog, state, auth_url } = this.config
    const p = new URLSearchParams({
      client_id, redirect_uri, response_type, state
    })
    p.set('scope', scopes.join(' '))
    show_dialog && p.set('show_dialog', 'true')
    const url = `${auth_url}?${p.toString()}`
    // console.log(url)
    const popup = window.open(url, '_blank', 'width=200');
    popup?.focus()
  }

  logout(){
    this.token = null    
    sessionStorage.removeItem('token')
  }

  extractToken() {
    const token = sessionStorage.getItem('token')
    if (token) {
      this.token = JSON.parse(token)
    }


    window.addEventListener('message', (msg: any) => {
      if (msg.data.type !== 'PLACKI_OAUTH') { return }
      this.token = msg.data.payload['access_token'];
      sessionStorage.setItem('token', JSON.stringify(this.token))
    })
  }

  getToken() {
    return this.token
  }

}