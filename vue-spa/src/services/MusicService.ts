import { Observable, EMPTY, NEVER, of, throwError, Subject, from, Subscription, ReplaySubject, BehaviorSubject } from "rxjs";
import { map, catchError, concat, mergeAll, concatAll, exhaust, switchAll, startWith, switchMap } from "rxjs/operators";
import { Album, AlbumsSearchResult } from '@/model/Search';
import { APIRequests } from './APIRequests';
import { HttpResponse } from 'vue-resource/types/vue_resource';


export class MusicService {
  api = new APIRequests()

  // private query = new BehaviorSubject<string>('batman')
  private query = new ReplaySubject<string>()
  queryChange = this.query.asObservable()

  private results = new BehaviorSubject<Album[]>([])
  resultsChange = this.results.asObservable()

  constructor() {
    (window as any).subject = this.results
    this.queryChange.pipe(
      switchMap(query => this.sendSearchRequest(query)),
    ).subscribe(this.results)
  }

  searchAlbums(query: string) {
    this.query.next(query)
  }

  sendSearchRequest(query: string): Observable<Album[]> {
    return this.api.get('search', {
      params: {
        type: 'album', q: query
      }
    }).pipe(
      map((reps: { data: AlbumsSearchResult }) => reps.data.albums.items),
      httpErrorHandler()
    )
  }

  getAlbumById(album_id: string) {
    return this.api.get(`albums/${album_id}`).pipe(
      map((reps: { data: Album }) => reps.data),
      httpErrorHandler()
    )
  }

}

export const httpErrorHandler = function <T>() {
  return (obs: Observable<T>) => obs.pipe(
    catchError((err: any, caught: Observable<T>) => {

      if (err.data && err.data.error) {
        return throwError(new Error(err.data.error.message))
      }
      return throwError(err)
    })
  )
}
    // return from([
    //   of(this.results),
    //   this.resultsChange.asObservable()
    // ]).pipe(
    //   mergeAll() // subsrcibe all inner observables at once,
    // // concatAll() // subscribe all in order,
    // // exhaust() // ignore others until current completes,
    // // switchAll() // unsubscibe current and subscribe next
    // )