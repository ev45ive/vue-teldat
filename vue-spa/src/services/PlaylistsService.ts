import { Playlist } from '@/model/Playlist';
import { APIRequests } from './APIRequests';
import { BehaviorSubject } from 'rxjs';
import { PagingObject } from '@/model/Search';

export class PlaylistsService {

  loadMyPlaylists() {
    this.getMyPlaylists()
      .subscribe((resp) => {
        const playlists = (resp.data as PagingObject<Playlist>).items
        this.playlists.next(playlists)
      })
  }
  api = new APIRequests()

  private playlists = new BehaviorSubject<Playlist[]>(mockPlaylistsData)
  playlists$ = this.playlists.asObservable()
  // selectedId = new BehaviorSubject<number | null>(234)


  getPlaylistById(id: Playlist['id']) {
    return this.api.get(`playlists/${id}`)
  }

  getMyPlaylists() {
    return this.api.get(`me/playlists/`)
  }

  createPlaylist(userId: string, draft: Playlist) {
    return this.api.post(`users/${userId}/playlists/`, draft)
  }

  updatePlaylist({
    id,
    name, 
    public: isPublic, 
    description
  }: Playlist) {
    return this.api.put(`playlists/${id}/`, {
      name, 
      public: isPublic, 
      description
    })
  }
}


export const mockPlaylistsData = [
  {
    id: 123,
    name: "Vue top20",
    public: true,
    description: "20 songs"
  },
  {
    id: 234,
    name: "BEst of Vue",
    public: false,
    description: "my fav songs"
  },
  {
    id: 345,
    name: "Vue Hits",
    public: true,
    description: "my fav songs"
  }
];