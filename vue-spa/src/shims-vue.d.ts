declare module "*.vue" {
  import Vue from "vue";
  export default Vue;
}

declare module "\*.png" {
  const path: string;
  export default path;
}

declare module "\*.jpg" {
  const path: string;
  export default path;
}

declare module "\*.json" {
  const path: string;
  export default path;
}


// declare module "vue-property-decorator" {
//   const Placki: number;
// }