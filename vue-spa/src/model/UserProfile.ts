import { ExternalUrls } from './Search';

export interface UserProfile {
  display_name:  string;
  external_urls: ExternalUrls;
  followers:     Followers;
  href:          string;
  id:            string;
  images:        any[];
  type:          string;
  uri:           string;
}


export interface Followers {
  href:  null;
  total: number;
}
