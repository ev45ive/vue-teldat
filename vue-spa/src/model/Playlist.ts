export interface Entity {
  id: number;
  name: string;
}

export interface Track extends Entity {
  duration: number
}


export interface Playlist extends Entity {
  public: boolean;
  description: string;
  // tracks: Array<Track>
  /**
   * List of tracks object
   * @todo placki
   */
  tracks?: Track[]
}

// const p: Playlist = {
//   id: 123,
//   name: 'test',
//   description: ',',
//   public: true
// }

// p.tracks && p.tracks.forEach
// p.tracks?.forEach
// p.tracks!.forEach

// typeof p.id == 'string' && p.id.search('')

// interface Point { kind: 'point', x: number; y: number }
// interface Vector { kind: 'vector', x: number; y: number, length: number }

// let p: Point = { kind: 'point', x: 123, y: 134 }
// let v: Vector = { kind: 'vector', x: 123, y: 134, length: 123 }
// let x: Point | Vector = {} as any
// // p = v;
// // v = p;

// if(x.kind == 'vector'){
//   x.length
// }else{
//   // x.length <= error, point type
// }

