import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "../views/Home.vue";
import Playlists from '../views/Playlists.vue'
import Search from '../views/Search.vue'
import TestForms from '../views/TestForms.vue'
import AlbumDetails from '../views/AlbumDetails.vue'
import PlaylistsNested from '../views/PlaylistsNested.vue'
import PlaylistsContainer from '../containers/PlaylistsContainer.vue'
import CreatePlaylist from '../containers/CreatePlaylist.vue'
import PlaylistDetailsContainer from '../containers/PlaylistDetailsContainer.vue'
import EditPlaylistContainer from '../containers/EditPlaylistContainer.vue'

Vue.use(VueRouter);

export enum Routes {
  Playlists = 'Playlists',
  EditPlaylist = 'EditPlaylist'
}


const routes: Array<RouteConfig> = [
  {
    path: '/',
    redirect: '/playlists',
  },
  {
    path: '/playlists',
    name: "Playlists",
    component: PlaylistsNested,
    children: [
      {
        path: '',
        components: {
          default: PlaylistsContainer
        }
      },
      {
        path: 'create',
        name: "CreatePlaylist",
        components: {
          default: PlaylistsContainer,
          details: CreatePlaylist
        }
      },
      {
        path: ':id',
        name: 'PlaylistDetails',
        components: {
          default: PlaylistsContainer,
          details: PlaylistDetailsContainer
        }
      },
      {
        path: ':id/edit',
        name: Routes.EditPlaylist,
        components: {
          default: PlaylistsContainer,
          details: EditPlaylistContainer
        }
      }
    ]
  },
  {
    path: "/home",
    name: "Home",
    component: Home
  },
  {
    path: "/search",
    name: "Search",
    component: Search
  },
  {
    path: "/album/:album_id",
    name: "AlbumDetails",
    component: AlbumDetails
  },
  {
    path: '/forms',
    component: TestForms,
    beforeEnter(to, from, next) { next() },
  },
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/About.vue")
  },
  {
    path: '**',
    redirect: '/home'
  }
];

const router = new VueRouter({
  mode: "history",
  // mode:'hash',
  base: process.env.BASE_URL,
  linkActiveClass: 'active',
  linkExactActiveClass: 'active-exact',
  routes
});

export default router;
