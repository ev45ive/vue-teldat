import Vue from "vue";
import App from "./App.vue";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import http from './resource'
import UserProfile from './containers/UserProfile.vue'
import VueRx from 'vue-rx'

Vue.config.productionTip = false;

Vue.use(VueRx,{})

Vue.filter('yesno', (value: boolean, yes = "yes", no = "no") => (value ? yes : no))
Vue.filter('titlecase', (v: string) => v.slice(0, 1).toUpperCase() + v.slice(1))
Vue.component('user-profile', UserProfile)


const app = new Vue({
  router,
  store,
  http,
  render: h => h(App)
})

  .$mount("#app");

