module.exports = {
  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel",
  "snapshotSerializers": [
    "<rootDir>/node_modules/jest-serializer-vue"
  ]
};
