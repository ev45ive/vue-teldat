# Git
git clone https://bitbucket.org/ev45ive/vue-teldat.git
cd vue-teldat/vue-simple
npm i 
npm start

# Versions
node -v 
v12.16.2

npm -v
6.14.4

code -v
1.40.1
8795a9889db74563ddd43eb0a897a2384129a619
x64

git --version
git version 2.23.0.windows.1

# VS CODE
https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode
https://marketplace.visualstudio.com/items?itemName=sdras.vue-vscode-snippets
https://marketplace.visualstudio.com/items?itemName=octref.vetur
https://marketplace.visualstudio.com/items?itemName=prograhammer.tslint-vue

# NPM
npm init -y
npm search nazwa pakietu
npm i http-server

node node_modules/http-server/bin/http-server
# package.json 
```json
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "start": "http-server"
  }, 
```
npm run nazwa_skryptu
npm start

# Vue CLI
https://cli.vuejs.org/

npm install -g @vue/cli

vue create vue-spa


Vue CLI v4.4.6
? Please pick a preset: Manually select features
? Check the features needed for your project: Babel, TS, PWA, Router, Vuex, CSS Pre-processors, Linter, Unit, E2E
? Use class-style component syntax? Yes
? Use Babel alongside TypeScript (required for modern mode, auto-detected polyfills, transpiling JSX)? Yes
? Use history mode for router? (Requires proper server setup for index fallback in production) Yes
? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default): Sass/SCSS (with dart-sass)
? Pick a linter / formatter config: Prettier
? Pick additional lint features:
? Pick a unit testing solution: Jest
? Pick an E2E testing solution: Cypress
? Where do you prefer placing config for Babel, ESLint, etc.? In dedicated config files
? Save this as a preset for future projects? Yes
? Save preset as: teldat


# Snippets
emmet:
.container>.row>.col

snippets:
https://jsfiddle.net/fvd8704j/
```
  "vueclass": {
    "prefix": "vueclass",
    "body": [
      "import { Component, Vue, Prop } from \"vue-property-decorator\";",
      "",
      "@Component({})",
      "export default class $1 extends Vue {$2}"
    ]
  }
```
https://code.visualstudio.com/docs/editor/userdefinedsnippets#_create-your-own-snippets


# Service workers / PWA
https://developers.google.com/web/tools/workbox/guides/codelabs/webpack
https://whatwebcando.today/
https://developers.google.com/web/tools/workbox/guides/using-plugins